
require 'win32console'

class StatusText
  def initialize(parms={})
    @previous_size = 0
    @stream = parms[:stream]==nil ? $stdout : parms[:stream]
    @parms = parms
    @parms[:verbose] = true if parms[:verbose] == nil
    @header = []
    @onChange = nil
    pushHeader(@parms[:base]) if @parms[:base]
  end

  def setText(complement)
    text = "#{@header.join(" ")}#{@parms[:before]}#{complement}#{@parms[:after]}"
    printText(text)
  end

  def cleanAll
    printText("")
  end

  def cleanContent
    printText "#{@parms[:base]}"
  end

  def nextLine(text=nil)
    if @parms[:verbose]
      @previous_size = 0
      @stream.print "\n"
    end
    if text!=nil
      line(text)
    end
  end

  def line(text)
    printText(text)
    nextLine
  end

  #Callback in the case the status text changes
  #might be useful to log the status changes
  #The callback function receives the new text
  def onChange(&block)
    @on_change = block
  end

  def pushHeader(head)
    @header.push(head)  
  end

  def popHeader
    @header.pop  
  end

  def setParm(parm, value)
    @parms[parm] = value
    if parm == :base
      @header.last = value
    end
  end

  private
  def printText(text)
    #If not verbose leave without printing
    if @parms[:verbose]
      if @previous_size > 0
        #go back
        @stream.print "\033[#{@previous_size}D"
        #clean
        @stream.print(" " * @previous_size) 
        #go back again
        @stream.print "\033[#{@previous_size}D"
      end
      #print
      @stream.print text
      @stream.flush
      #store size
      @previous_size = text.gsub(/\e\[\d+m/,"").size
    end
    #Call callback if existent
    @on_change.call(text) if @on_change
  end
end

#a = StatusText.new(:before => "Evolution (", :after => ")")

#(1..100).each {|i| a.setText(i.to_s); sleep(1)}
#a.nextLine